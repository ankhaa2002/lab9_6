import * as React from "react";
import { CheckBox } from "react-native-elements";

export default () => {
  const [checked, setChecked] = React.useState(false);
  return (
    <CheckBox
      checked={checked}
      checkedColor="#0F0"
      checkedTitle="Check лэгдлээ"
      containerStyle={{ width: "75%", marginTop: 50 }}
      onIconPress={() => setChecked(!checked)}
      size={50}
      textStyle={{}}
      title="Check лэнэ үү?"
      titleProps={{}}
      uncheckedColor="#F00"
    />
  );
};